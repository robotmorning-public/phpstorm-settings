#set($localClassName = $ClassName)
#set($localParentClassName = $ParentClass)
USS.defineClass(
    '${localClassName}',
    [],
    function()
    {
        /**
         * <p>$Description</p>
         *
         * @constructor
        #if(${localParentClassName} && ${localParentClassName} != "")
         * @extends ${localParentClassName}
        #end
         * 
         * @author ${USER}
         * @copyright Robot Morning, LLC
         * 
         * @param {Object} options
         */
        ${localClassName}= function(options)
        {
            if(options !== false)
            {
                this.__construct(options);
            }
        };
        
        #if(${localParentClassName} && ${localParentClassName} != "")
        // Extending an existing class
        ${localClassName}.prototype = new ${localParentClassName}(false);
        ${localClassName}.prototype.constructor = ${localClassName};
        ${localClassName}.parent = ${localParentClassName}.prototype;
        
        #end
        /**
         * Processes constructor options
         * @access protected
         * @param {Object} options
         */
        ${localClassName}.prototype.__construct = function(options)
        {
        #if(${localParentClassName} && ${localParentClassName} != "")
            //call the parent constructor
            ${localClassName}.parent.__construct.call(this, options);
        #end
        };
        
        /**
         * Destroys the object
         * @access protected
         */
        ${localClassName}.prototype.__destruct = function()
        {
        #if(${localParentClassName} && ${localParentClassName} != "")
            //call the parent destructor
            ${localClassName}.parent.__destruct.call(this);
        #end
        };
    }
);